/*
Szabo Robert
srim1761
514
*/
#include <iostream>
#include "splay.h"
#include <time.h>
#include <fstream>
#include  <strings.h>

using namespace std;
bool a[105];

struct elem
{
  string nev;
  long long szam;
};

void kiir(splay<elem>* gyoker)
{
  cout << "Hazsam: " << gyoker->kulcs << endl;
  cout << "Nev: " << gyoker->adat.nev << endl;
  cout << "Telefonszam: " << gyoker->adat.szam << endl << endl;
}

void beolvas(splay<elem> *&gyoker,SplayTree<elem> &st)
{
  int n=0;
  do
  {
    cout << "Hany lakost szeretne? ";
    cin >>n;
  }
  while (n>50);
  ifstream f;
  int k=rand()%100+1;
  f.open("input.txt");
  for (int i=1;i<=n;i++)
  {
    elem tmp;
    f >> tmp.nev;
    f >> tmp.szam;
    gyoker=st.Insert(k,gyoker,tmp);
    a[k]=false;
    do
    {
      k=rand()%100+1;
    }
    while(!a[i]);
  }
}

elem newelem()
{
  elem seged;
  cout << "Szemely neve? ";
  cin >> seged.nev;
  cout << "Szemely telefonszama? ";
  cin >> seged.szam;
  cout << endl;
  return seged;
}


void teljeskiir(SplayTree<elem> st,splay<elem> *gyoker)
{
  st.InOrder(gyoker,kiir);
}

void prefkiir(SplayTree<elem> st,splay<elem> *gyoker)
{
  st.PreOrder(gyoker,kiir);
}

int main()
{
    for (int i=1;i<=100;i++)
      a[i]=true;
    SplayTree<elem> st;
    splay<elem> *gyoker;
    gyoker = NULL;
    int i;
    int input, choice,choice2;
    while(1)
    {
        cout<<"1. Hozzaad "<<endl;
        cout<<"2. Torol"<<endl;
        cout<<"3. Feltoltes allomanybol"<<endl;
        cout<<"4. Keres"<<endl;
        cout<<"5. Kiir"<<endl;
        cout<<"6. Kilep"<<endl;
        cout<<"Az on valasztasa: ";
        cin>>choice;
        cout << endl;
        elem tem;
        switch(choice)
        {
        case 1:
            cout<<"Uj lakos hazszama: ";
            cin>>input;
            tem=newelem();
            a[input]=false;
            gyoker = st.Insert(input, gyoker, tem);
            break;
        case 2:
            cout<<"Torlendo hazszam : ";
            cin>>input;
            cout << endl;
            a[input]=true;
            gyoker = st.Delete(input, gyoker);
            break;
        case 3:
            beolvas(gyoker,st);
            cout << endl;
            break;
        case 4:
            cout << "1. Hazszam szerint" << endl;
            cout << "2. Legnepszerubbek" << endl;
            cout << "Valasz : " ;
            cin >> choice2;
            cout << endl;
            if (choice2==1)
            {
              cout<<"Keresett hazsam: ";
              cin>>input;
              cout << endl;
              gyoker = st.Search(input, gyoker);
              kiir(gyoker);
            }
            if (choice2==2)
            {
              kiir(gyoker);
              if (gyoker->jobb)
              kiir(gyoker->jobb);
              if (gyoker->bal)
              kiir(gyoker->bal);
            }
            break;

        case 5:
            cout << "1. Hazszam szerint" << endl;
            cout << "2. Legkeresettebbel elore" << endl;
            cout << "Valasz : " ;
            cin >> choice2;
            cout << endl;
            if (choice2==1)
              teljeskiir(st,gyoker);
            if (choice2==2)
              prefkiir(st,gyoker);
            break;

        case 6:

          system("cls");
            exit(1);
        default:
            cout<<"\nRossz bemenet! \n";
        }
    }
    cout<<"\n";
    return 0;
}
