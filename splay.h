/*
Szabo Robert
srim1761
514
*/
#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

template <typename T>
struct splay
{
    int kulcs;
    T adat;
    splay* bal;
    splay* jobb;
};

template <typename T>
class SplayTree
{
    public:
        SplayTree()
        {
        }

        // Forgatas jobbra
        splay<T>* jobbraforgat(splay<T>* k2)
        {
            splay<T>* k1 = k2->bal;
            k2->bal = k1->jobb;
            k1->jobb = k2;
            return k1;
        }

        // Forgatas balra
        splay<T>* balraforgat(splay<T>* k2)
        {
            splay<T>* k1 = k2->jobb;
            k2->jobb = k1->bal;
            k1->bal = k2;
            return k1;
        }

        splay<T>* Splay(int kulcs, splay<T>* gyoker)
        {
            if (!gyoker)
                return NULL;
            splay<T> header;
            header.bal = header.jobb = NULL;
            splay<T>* BalFaMax = &header;
            splay<T>* JobbFaMin = &header;
            while (1)
            {
                if (kulcs < gyoker->kulcs)
                {
                    if (!gyoker->bal)
                        break;
                    if (kulcs < gyoker->bal->kulcs)
                    {
                        gyoker = jobbraforgat(gyoker);
                        if (!gyoker->bal)
                            break;
                    }
                    JobbFaMin->bal = gyoker;
                    JobbFaMin = JobbFaMin->bal;
                    gyoker = gyoker->bal;
                    JobbFaMin->bal = NULL;
                }
                else if (kulcs > gyoker->kulcs)
                {
                    if (!gyoker->jobb)
                        break;
                    if (kulcs > gyoker->jobb->kulcs)
                    {
                        gyoker = balraforgat(gyoker);
                        if (!gyoker->jobb)
                            break;
                    }
                    BalFaMax->jobb = gyoker;
                    BalFaMax = BalFaMax->jobb;
                    gyoker = gyoker->jobb;
                    BalFaMax->jobb = NULL;
                }
                else
                    break;
            }
            BalFaMax->jobb = gyoker->bal;
            JobbFaMin->bal = gyoker->jobb;
            gyoker->bal = header.jobb;
            gyoker->jobb = header.bal;
            return gyoker;
        }

        splay<T>* New_Node(int kulcs)
        {
            splay<T>* p = new splay<T>;
            if (!p)
            {
                cout <<"A memoria megtelt!" << endl;
                exit(1);
            }
            p->kulcs = kulcs;
            p->bal = p->jobb = NULL;
            return p;
        }

        splay<T>* Insert(int kulcs, splay<T>* gyoker, T adat)
        {
            static splay<T>* p = NULL;
            if (!p)
                p = New_Node(kulcs);
            else
                p->kulcs = kulcs;
                p->adat = adat;
            if (!gyoker)
            {
                gyoker = p;
                p = NULL;
                return gyoker;
            }
            gyoker = Splay(kulcs, gyoker);
            if (kulcs < gyoker->kulcs)
            {
                p->bal = gyoker->bal;
                p->jobb = gyoker;
                gyoker->bal = NULL;
                gyoker = p;
            }
            else if (kulcs > gyoker->kulcs)
            {
                p->jobb = gyoker->jobb;
                p->bal = gyoker;
                gyoker->jobb = NULL;
                gyoker = p;
            }
            else
                return gyoker;
            p = NULL;
            return gyoker;
        }

        splay<T>* Delete(int kulcs, splay<T>* gyoker)
        {
            splay<T>* temp;
            if (!gyoker)
                return NULL;
            gyoker = Splay(kulcs, gyoker);
            if (kulcs != gyoker->kulcs)
                return gyoker;
            else
            {
                if (!gyoker->bal)
                {
                    temp = gyoker;
                    gyoker = gyoker->jobb;
                }
                else
                {
                    temp = gyoker;
                    gyoker = Splay(kulcs, gyoker->bal);
                    gyoker->jobb = temp->jobb;
                }
                free(temp);
                return gyoker;
            }
        }

        splay<T>* Search(int kulcs, splay<T>* gyoker)
        {
            return Splay(kulcs, gyoker);
        }

        void InOrder(splay<T>* gyoker,void (ki)(splay<T>* gyoker))
        {
            if (gyoker)
            {
                InOrder(gyoker->bal,ki);
                ki(gyoker);
                InOrder(gyoker->jobb,ki);
            }
        }

        void PreOrder(splay<T>* gyoker,void (ki)(splay<T>* gyoker))
        {
            if (gyoker)
            {
                ki(gyoker);
                InOrder(gyoker->bal,ki);
                InOrder(gyoker->jobb,ki);
            }
        }

};
